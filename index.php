<?php

require_once("class_grid.php");
require_once("class_sudoku.php");

$method = "table";

$rawData = <<<EOB
075000010
091004670
080670900
900030700
010060090
006040005
008092030
069400120
020000840
EOB;

$rawData = <<<EOB
000000000
000000000
000000000
000000000
000000000
000000000
000000000
000000000
000000000
EOB;

$showOutput = false;

if ($method == "raw") {
	if (isset($_POST["sudoku"])) {
		$rawData = $_POST["sudoku"];
		$showOutput = true;
	}
}

if ($method == "table") {
	if (isset($_POST["submitTable"])) {
		$rawData = "";

		for ($j = 0; $j < 9; $j++) {
			if ($j > 0) {
				$rawData .= "\n";
			}
			for ($i = 0; $i < 9; $i++) {
				if (isset($_POST["sudoku_{$i}_{$j}"])) {
					$val = $_POST["sudoku_{$i}_{$j}"];
					if ($val == "") {
						$val = "0";
					}

					$rawData .= $val;
				} else {
					$rawData .= "0";
				}
			}
		}

		$showOutput = true;
	}
}

$data = explode("\n", $rawData);


$grid = new grid(9, 9, 0);

for ($j = 0; $j < 9; $j++) {
	$line = $data[$j];
	for ($i = 0; $i < 9; $i++) {
		if ($i < strlen($line)) {
			$val = $line[$i];
		} else {
			$val = 0;
		}

		if ( ($val != 1) and ($val != 2) and ($val != 3) and
			 ($val != 4) and ($val != 5) and ($val != 6) and
			 ($val != 7) and ($val != 8) and ($val != 9)
		   ) {
			$grid->set($i, $j, 0);
		} else {
			$grid->set($i, $j, $val);
		}
	}
}

if ($showOutput) {
	$time = -microtime(true);
	$sudoku = new sudoku();
	$solved = $sudoku->solve($grid);
	$time += microtime(true);
}

?>

<!DOCTYPE html>
<html>
<head>
<title>Sudoku solver</title>
<style>
body {
	font-family: sans-serif;
}

textarea {
	width: 150px;
	height: 230px;
	font-family: monospace;
	font-size: 15pt;
}

input[type="text"] {
	width: 25px;
	height: 25px;
	text-align: center;
	border-style: none;
	font-family: monospace;
	font-size: 13pt;
	color: black;
}

td {
	width: 25px;
	height: 25px;
	text-align: center;
	font-family: monospace;
	font-size: 13pt;
	border-style: solid;
	border-width: 1px;
}

table tr:nth-of-type(3n+1) td {
	border-top-style: solid;
	border-top-width: 2pt;
	border-top-color: black;
}

table tr td:nth-of-type(3n+1) {
	border-left-style: solid;
	border-left-width: 2pt;
	border-left-color: black;
}

table tr:nth-of-type(3n) td {
	border-bottom-style: solid;
	border-bottom-width: 2pt;
	border-bottom-color: black;
}

table tr td:nth-of-type(3n) {
	border-right-style: solid;
	border-right-width: 2pt;
	border-right-color: black;
}

a {
	color: blue;
}
</style>
<script type="text/javascript">
<!--
var downStrokeField;
function autojump(fieldName,nextFieldName,fakeMaxLength)
{
var myForm=document.forms[document.forms.length - 1];
var myField=myForm.elements[fieldName];
myField.nextField=myForm.elements[nextFieldName];

if (myField.maxLength == null)
   myField.maxLength=fakeMaxLength;

myField.onkeydown=autojump_keyDown;
myField.onkeyup=autojump_keyUp;
}

function autojump_keyDown()
{
this.beforeLength=this.value.length;
downStrokeField=this;
}

function autojump_keyUp()
{
if (
   (this == downStrokeField) && 
   (this.value.length > this.beforeLength) && 
   (this.value.length >= this.maxLength)
   )
   this.nextField.focus();
downStrokeField=null;
}
//-->
</script>
</head>
<body>

<h1>Sudoku solver</h1>

<h2>Set sudoku</h2>

<form method="post" action="">

<?php


switch ($method) {
	case "raw":
?>
Enter sudoku details (use 0 for unknown cells):<br>
<textarea name="sudoku"><?php echo($rawData); ?></textarea>
<br><br>
<?php
	break;

	case "table":
	echo("<table>");
	// $previous = "";
	// $autoJump = array();
	for ($j = 0; $j < 9; $j++) {
		echo("<tr>");
		for ($i = 0; $i < 9; $i++) {
			$val = $data[$j][$i];
			if ($val == 0) {
				$val = "";
			}
			echo("<td><input type=\"text\" name=\"sudoku_{$i}_{$j}\" value=\"" . $val . "\" maxlength=\"1\"></td>");
			// $autoJump[] = [$previous, "sudoku_{$i}_{$j}"];
			// $previous = "sudoku_{$i}_{$j}";
		}
		echo("</tr>");
	}
	echo("</table>");
	echo("<input type=\"hidden\" name=\"submitTable\" value=\"yes\">");
	/*
	?>
	<SCRIPT TYPE="text/javascript">
	<!--
	<?php
	foreach ($autoJump as $jump) {
		$a = $jump[0];
		$b = $jump[1];
		if ($a != "") {
			echo("autojump('" . $a . "', '" . $b . "', 1);\n");
		}
	}
	?>
	//-->
	</SCRIPT>
	<?php
	*/
	break;
}
?>
<input type="submit" value="Solve">
</form>
<?php

if ($showOutput) {
?>
	<hr>

	<h2>Sudoku result</h2>

	<?php

	if ($solved) {
		echo("<p>This sudoku was solved succesfully!</p>");
	} else {
		echo("<p>This sudoku was not solved by this program, I'm sorry. It has been partially solved.</p>");
	}
	?>

	<?php $grid->output(); ?>

	<p>Solved in <?php echo($time); ?> seconds.</p>
	<p>Needed <?php echo($sudoku->phases); ?> phase(s) to make this solution.</p>

<?php
}
?>

<hr>

<p><a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Sudoku Solver</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://www.bjarno.be/" property="cc:attributionName" rel="cc:attributionURL">Bjarno Oeyen</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.</p>

<p>You can find the source code of this project on <a href="https://bitbucket.org/Bjarnovikus/sudoku-solver" target="_blank">Bitbucket</a>.</p>

</body>
</html>