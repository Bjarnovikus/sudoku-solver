<?php

require_once("class_grid.php");

class sudoku {
	public $sudoku = array();
	public $possibilities = array();
	public $numberOfSolvedCells = 81;
	public $solved = false;
	public $phases = 0;

	public function solve($sudoku) {

		// We set the class attributes

		$this->sudoku = $sudoku;
		$this->possibilities = new grid(9, 9, []);
		$this->numberOfSolvedCells = 81;


		// We generate the default possibilities (1-9) for all unsolved cells

		$this->setInitialPossibilities();
		

		// We keep track of the current number of solved cells and phases

		$old = 81;
		$this->phases = 0;

		$this->solved = false;


		// While the grid is unsolved (number of solved cells not equal to 81), we keep on solving
		
		while ($this->numberOfSolvedCells != 81) {
			if ($old == $this->numberOfSolvedCells) {
				return false;
			}

			$old = $this->numberOfSolvedCells;

			// Generate the possible values for each cell

			$this->generatePossibilities();
			

			// When cells have only 1 possibility, apply it

			$this->applySinglePossibilities();


			// Check whether one of the possibilities of a still unsolved cell, is the only possible one
			// in its row, column or sector.

			$this->checkOnlyPossibility();


			// In the previous step, we reuse the old possibilities when a cell has been changed for later
			// cells. This step will reset all solved cells back to an empty possibility array.

			$this->resetPossibilities();


			// Increase the number of phases

			$this->phases++;
		}

		$this->solved = true;

		return true;
	}

	private function isSolved($i, $j) {
		return ($this->sudoku->get($i, $j) != 0);
	}

	private function setInitialPossibilities() {
		for ($j = 0; $j < 9; $j++) {
			for ($i = 0; $i < 9; $i++) {
				if ($this->sudoku->get($i, $j) == 0) {
					$this->possibilities->set($i, $j, [1, 2, 3, 4, 5, 6, 7, 8, 9]);
					$this->numberOfSolvedCells -= 1;
				}
			}
		}
	}

	private function generatePossibilities() {
		for ($j = 0; $j < 9; $j++) {
			for ($i = 0; $i < 9; $i++) {
				$new = [];
				foreach ($this->possibilities->get($i, $j) as $possibility) {
					if ($this->canPlace($i, $j, $possibility)) {
						$new[] = $possibility;
					}
				}
				$this->possibilities->set($i, $j, $new);
			}
		}
	}

	private function applySinglePossibilities() {
		for ($j = 0; $j < 9; $j++) {
			for ($i = 0; $i < 9; $i++) {
				if (!($this->isSolved($i, $j))) {
					$possibilities = $this->possibilities->get($i, $j);

					if (count($possibilities) == 1) {
						// Extra check to make no errors when solving an badly formed sudoku puzzle.
						if ($this->canPlace($i, $j, $possibilities[0])) {
							$this->sudoku->set($i, $j, $possibilities[0]);
							$this->numberOfSolvedCells += 1;
						}
					}
				}
			}
		}
	}

	private function checkOnlyPossibility() {
		for ($j = 0; $j < 9; $j++) {
			for ($i = 0; $i < 9; $i++) {
				if (!($this->isSolved($i, $j))) {
					$possibilities = $this->possibilities->get($i, $j);
					$skip = false;

					foreach ($possibilities as $possibility) {
						if ($skip == false) {
							if ( ($this->possibilityOccurancesRow($j, $possibility) == 1) or
								 ($this->possibilityOccurancesColumn($i, $possibility) == 1) or
								 ($this->possibilityOccurancesSector($this->getSector($i, $j), $possibility)) == 1) {
								$this->sudoku->set($i, $j, $possibility);
								$this->numberOfSolvedCells += 1;
								$skip = true;
							}
						}
					}
				}
			}
		}
	}

	private function resetPossibilities() {
		for ($j = 0; $j < 9; $j++) {
			for ($i = 0; $i < 9; $i++) {
				if ($this->isSolved($i, $j)) {
					$this->possibilities->set($i, $j, []);
				}
			}
		}
	}

	private function getSector($i, $j) {
		return [floor($i / 3), floor($j / 3)];
	}

	private function canPlace($i, $j, $possibility) {
		return
		(!( ($this->inRow($j, $possibility)) or
			($this->inColumn($i, $possibility)) or
			($this->inSector($this->getSector($i, $j), $possibility))
		));
	}

	private function inRow($j, $possibility) {
		for ($i = 0; $i < 9; $i++) {
			if ($this->sudoku->get($i, $j) == $possibility) {
				return true;
			}
		}

		return false;
	}

	private function inColumn($i, $possibility) {
		for ($j = 0; $j < 9; $j++) {
			if ($this->sudoku->get($i, $j) == $possibility) {
				return true;
			}
		}

		return false;
	}

	private function inSector($sector, $possibility) {
		$a = $sector[0];
		$b = $sector[1];

		for ($j = $b * 3; $j < ($b + 1) * 3; $j++) {
			for ($i = $a * 3; $i < ($a + 1) * 3; $i++) {
				if ($this->sudoku->get($i, $j) == $possibility) {
					return true;
				}
			}
		}

		return false;
	}

	private function possibilityOccurancesRow($j, $possibility) {
		$count = 0;

		for ($i = 0; $i < 9; $i++) {
			if (in_array($possibility, $this->possibilities->get($i, $j))) {
				$count++;
			}
		}

		return $count;
	}

	private function possibilityOccurancesColumn($i, $possibility) {
		$count = 0;

		for ($j = 0; $j < 9; $j++) {
			if (in_array($possibility, $this->possibilities->get($i, $j))) {
				$count++;
			}
		}

		return $count;
	}

	private function possibilityOccurancesSector($sector, $possibility) {
		$count = 0;

		$a = $sector[0];
		$b = $sector[1];

		for ($j = $b * 3; $j < ($b + 1) * 3; $j++) {
			for ($i = $a * 3; $i < ($a + 1) * 3; $i++) {
				if (in_array($possibility, $this->possibilities->get($i, $j))) {
					$count++;
				}
			}
		}

		return $count;
	}
}