<?php

class grid {
	public $width;
	public $height;
	public $default;
	protected $data;

	public function __construct($width, $height, $default=NULL) {
		$width = (int) $width;
		$height = (int) $height;

		for ($i = 0; $i < $width; $i++) {
			for ($j = 0; $j < $height; $j++) {
				$this->data[$i][$j] = $default;
			}
		}

		$this->width = $width;
		$this->height = $height;
	}

	public function get($x, $y) {
		$x = (int) $x;
		$y = (int) $y;

		if ( ($x >= 0) and ($y >= 0) and ($x < $this->width) and ($y < $this->height) ) {
			return $this->data[$x][$y];
		} else {
			throw new Exception("hulp");
		}
	}

	public function set($x, $y, $value) {
		$x = (int) $x;
		$y = (int) $y;

		if ( ($x >= 0) and ($y >= 0) and ($x < $this->width) and ($y < $this->height) ) {
			$this->data[$x][$y] = $value;

			return true;
		}

		return false;
	}

	public function output() {
		echo("<table>");

		for ($j = 0; $j < $this->height; $j++) {
			echo("<tr>");
			for ($i = 0; $i < $this->width; $i++) {
				echo("<td><input type=\"text\" disabled value=\"");
				$val = $this->get($i, $j);
				if ($val != 0) {
					echo($this->get($i, $j));
				}
				echo("\">");
				echo("</td>");
			}
			echo("</tr>");
		}

		echo("</table>");
	}

	public function dump() {
		echo("<table>");

		for ($j = 0; $j < $this->height; $j++) {
			echo("<tr>");
			for ($i = 0; $i < $this->width; $i++) {
				echo("<td>");
				echo(json_encode($this->get($i, $j)));
				echo("</td>");
			}
			echo("</tr>");
		}

		echo("</table>");
	}
}