# Sudoku Solver

A Sudoku solver written in PHP in a object-oriented style. This features a simple web application where users can enter their difficult sudoku puzzles.


## Difficulty

I've tried the solver with several sudokoes with difficulties ranging from easy to hard. I've tested them with sudokoes from http://www.websudoku.com/. The solver has some problems with the evil difficulty, but I'm hoping to find some way to solve these as well.


## License

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
